# Talks

Code + slides for all of my talks

- [My Journey Using Docker as a Development Tool](https://docker-as-a-dev-tool.haseebmajid.dev)
- [An Introduction to Pocketbase:  A Go-Based Backend as a Service](https://an-intro-to-pocketbase.haseebmajid.dev)

## Appendix

- "Talks links page" inspired by: https://github.com/SamirPaul1/links
- Icons from https://www.flaticon.com/